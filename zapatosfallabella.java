package zapatos;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utilities.GetProperties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.interactions.Actions;
/*Automatizar la prueba funcional del siguiente flujo:

        1.- Entrar a: https://www.falabella.com/falabella-cl/
        2.- Ir a “CATEGORÍAS”, luego “Zapatos”, y hacer clic en “Zapatos de seguridad”
        3.- Filtrar la búsqueda por “Botas”
        4.- Seleccionar producto y talla (Cualquiera)
        4.- Clic en "AGREGAR A LA BOLSA"
        5.- Aumentar la cantidad hasta 4 (hacer clic en el botón “+” hasta llegar a 4)
        6.- Clic en "Ver bolsa de compras", para desplegar el detalle de la bolsa de compras
        7.- Culminar la automatización (No continuar con la compra)
*/
public class zapatosfallabella {
    protected WebDriver driver;
    public static String URL="https://www.falabella.com/falabella-cl";

    @BeforeMethod
    public void setup() {
        GetProperties properties = new GetProperties();
        String chromeDriverUrl = properties.getString("CHROMEDRIVER_PATH");
        System.setProperty("webdriver.chrome.driver", chromeDriverUrl);
        driver = new ChromeDriver();

        driver.get(URL);
        // maximizar
        driver.manage().window().maximize();
        // metodo de espera implicito
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void flujo(){
     // cerrar venta emergente
        driver.findElement(By.xpath("//div[@class='dy-lb-close']")).click();

     // presionar en categoria
        driver.findElement(By.className("HamburgerBtn-module_icon-container__12faL")).click();

        // se lleva la elemento zapatos
 WebElement zapatos = driver.findElement(By.xpath("//p[contains(text(),'Zapatos')]"));

        Actions actionProvider = new Actions(driver);
        actionProvider.moveToElement(zapatos).build().perform();

        // zapatos de seguridad
      driver.findElement(By.xpath("//a[contains(text(),'Zapatos de seguridad')]")).click();

      //  Filtrar la búsqueda por “Botas”
    WebElement buscar= driver.findElement(By.id("testId-SearchBar-Input"));
    buscar.sendKeys("Botas");

    //
    driver.findElement(By.xpath("//*[@id=\"acc-alert-close\"]")).click();

    // boton buscar
    driver.findElement(By.xpath("//button[@class='SearchBar-module_searchBtnIcon__6KVum']//img")).click();

    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    // selecionar bota
     driver.findElement(By.id("testId-pod-displaySubTitle-882076657")).click();

      // elige opciones
     driver.findElement(By.xpath("//div[@id='buttonForCustomers']")).click();

        // talla 39  //button[@id='testId-sizeButton-39']
         driver.findElement(By.xpath("//div[contains(@class,'jsx-2264719937 size-variants')]//button[@id='testId-sizeButton-39']")).click();

        // agregar al carro
        driver.findElement(By.xpath("//button[@class='jsx-2166277967 button button-primary button-primary-medium']")).click();

        // boton mas
        WebElement botonMas= driver.findElement(By.xpath("//button[@class='jsx-635184967 increment']"));
        Actions actionProvider3 = new Actions(driver);
        // Realiza la acción double-click en el elemento
        actionProvider3.doubleClick(botonMas).build().perform();

        botonMas.click();
        botonMas.click();
      // ir a carro de compras
    driver.findElement(By.id("linkButton")).click();

      //  JavascriptExecutor js = (JavascriptExecutor) driver;
       // js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }



}
